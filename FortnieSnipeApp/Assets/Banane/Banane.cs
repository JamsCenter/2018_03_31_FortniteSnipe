using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/// <summary>
/// Script write by Eloi Stree (DefaultCompany). 
/// Check the following link to acknowledge the license.
/// License: "https://www.patreon.com/jamscenter"
/// Created: 4/4/2018 4:08:42 PM 
/// </summary>
[HelpURL("https://www.patreon.com/jamscenter")]
public class Banane : MonoBehaviour {

    public enum Language{ fr, en, de}

    public Language _from;
    public Language _to;

    public string _frenchWordToLook = "Banane";

}
