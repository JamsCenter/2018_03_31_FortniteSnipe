using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;



/// <summary>
/// Script write by Eloi Stree (DefaultCompany). 
/// Check the following link to acknowledge the license.
/// License: "https://www.patreon.com/jamscenter"
/// Created: 4/4/2018 4:10:49 PM 
/// </summary>
[CustomEditor(typeof(Banane))]
public class BananeEditor : Editor
{
    public override void OnInspectorGUI()
    {

        Banane myScript = (Banane) target;
        if (GUILayout.Button("Banana Phone"))
        {
            Application.OpenURL("https://translate.google.com/?hl=fr#"+ myScript ._from+ "/"+ myScript ._to+ "/"+ myScript._frenchWordToLook);
        }

        DrawDefaultInspector();
    }
}