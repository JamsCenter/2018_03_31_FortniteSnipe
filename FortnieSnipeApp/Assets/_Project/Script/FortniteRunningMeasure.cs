using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//#JAMSCENTER#
/// <summary>
/// Script write by Eloi Stree (DefaultCompany). 
/// Check the following link to acknowledge the license.
/// License: "https://www.patreon.com/jamscenter"
/// Created: 3/31/2018 4:06:37 PM 
/// </summary>
[HelpURL("https://www.patreon.com/jamscenter")]
public class FortniteRunningMeasure : MonoBehaviour
{

    public float _runningTime = 56f;
    public RectTransform _startPosition;
    public RectTransform _endPosition;

    [Header("Debug (Don't touch)")]
    [SerializeField]
#pragma warning disable CS0414  
    private float _unityDistance;


    public static FortniteRunningMeasure _instanceInScene;
    public static FortniteRunningMeasure _i;
    public void Awake()
    {
        _instanceInScene = this;
        _i = this;
    }

    public static float GetRunningTimeBetween(RectTransform start, RectTransform end)
    {
        if (start == null || end == null)
            throw new NotMyResponsabilityException("Please fill correclty the parameters");

        float result = 0;
        float runningTime = _i.GetRunningTime();
        float runningUnityMeasure = _i.GetUnityDistance();
        float askedUnityMeasure = Vector3.Distance(Position(start), Position(end));

        result = (runningTime / runningUnityMeasure) * askedUnityMeasure;

        return result;
    }

    private float GetRunningTime()
    {
        return _runningTime = 0;
    }

    private float GetUnityDistance()
    {
        if (ClassFieldNotValide()) return 0;
        return Vector3.Distance(Position(_startPosition), Position(_endPosition));
    }

    private bool ClassFieldNotValide()
    {

            return _startPosition == null || _endPosition == null; 
    }

    private void OnValidate()
    {
        _unityDistance = GetUnityDistance();
        Debug.DrawLine(Position(_startPosition), Position(_endPosition), Color.green, 60);
    }

    public static Vector3 Position(RectTransform transform) {
        return transform.position;
    }
}
