using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Script write by Eloi Stree (DefaultCompany). 
/// Check the following link to acknowledge the license.
/// License: "https://www.patreon.com/jamscenter"
/// Created: 3/31/2018 6:54:32 PM 
/// </summary>
[HelpURL("https://www.patreon.com/jamscenter")]
public class MovePlayerWhenPresssingDown : MonoBehaviour {

    public Image _panelToInteractWith;
    public RectTransform _panelToMove;

    public GraphicRaycaster m_Raycaster;
    public PointerEventData m_PointerEventData;
    public EventSystem m_EventSystem;

    public Vector2 pos;

    void Start()
    {
        if(m_Raycaster==null)
        m_Raycaster = GetComponent<GraphicRaycaster>();

        if (m_EventSystem == null)
            m_EventSystem = GetComponent<EventSystem>();
    }

    void Update()
    {
        if (Input.touchCount > 0 || Input.GetMouseButton(0))
        {
            if (_panelToInteractWith.Raycast( Input.mousePosition, Camera.main))
            {
          
                RectTransform rect = _panelToMove;
                
                Vector2  viewPos = Input.mousePosition;
                viewPos.x /= Screen.width;
                viewPos.y /= Screen.height;
                // clamp the grenade to the screen boundaries
                viewPos.x = Mathf.Clamp01(viewPos.x);
                viewPos.y = Mathf.Clamp01(viewPos.y);

                Vector2 pos = new Vector2(((viewPos.x * rect.sizeDelta.x) - (rect.sizeDelta.x * 0.5f)),
                                                        ((viewPos.y * rect.sizeDelta.y) - (rect.sizeDelta.y * 0.5f)));
                //    now you can set the position of the ui element
                _panelToMove.anchoredPosition = pos;
                
            }
        
        }
    }
}
