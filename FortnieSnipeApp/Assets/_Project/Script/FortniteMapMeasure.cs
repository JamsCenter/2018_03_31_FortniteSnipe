using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/// <summary>
/// Script write by Eloi Stree (DefaultCompany). 
/// Check the following link to acknowledge the license.
/// License: "https://www.patreon.com/jamscenter"
/// Created: 3/31/2018 5:05:37 PM 
/// </summary>
[HelpURL("https://www.patreon.com/jamscenter")]
public class FortniteMapMeasure : MonoBehaviour
{

    public RectTransform _startPosition;
    public RectTransform _endPosition;

    [Header("Debug (Don't touch)")]
    [SerializeField]
    private float _unityDiameter;
    private float _unityHorizontalDistance;
    private float _unityVerticalDistance;


    public static FortniteMapMeasure _instanceInScene;
    public static FortniteMapMeasure _i;

    public enum MeasureType { Horizontal, Vertical, Both }
    public void Awake()
    {
        _instanceInScene = this;
        _i = this;
    }

    public static float GetPourcentOf(RectTransform point, MeasureType measureType)
    {
        if (point == null)
            throw new NotMyResponsabilityException("Please fill correclty the parameters");

        float result = 0;
        float mapUnityMeasure = GetDistanceAsked(_i._endPosition, measureType);
        float askedUnityMeasure = GetDistanceAsked(point, measureType);

        result = askedUnityMeasure / mapUnityMeasure;

        return result;
    }

    private static float GetDistanceAsked(RectTransform point, MeasureType measureType)
    {
        float result = 0;
        switch (measureType)
        {
            case MeasureType.Horizontal:
                result = GetDistanceBetween(Position(_i._startPosition).x, Position(point).x);
                break;
            case MeasureType.Vertical:
                result = GetDistanceBetween(Position(_i._startPosition).y, Position(point).y);
                break;
            case MeasureType.Both:
                result = Vector3.Distance(Position(_i._startPosition), Position(point));
                break;
            default:
                break;
        }
        return result;
    }

    private static float GetDistanceBetween(float a, float b)
    {
        return Math.Abs(a - b);
    }

    private float GetUnityDistance()
    {
        if (ClassFieldNotValide()) return 0;
        return Vector3.Distance(Position(_startPosition), Position(_endPosition));
    }

    private bool ClassFieldNotValide()
    {

        return _startPosition == null || _endPosition == null;
    }

    private void OnValidate()
    {
        Vector3 start = Position(_startPosition);
        Vector3 endVertical = Position(_endPosition);
        endVertical.y = start.y;
        Vector3 endHorizontal = Position(_endPosition);
        endHorizontal.x = start.x;
        Debug.DrawLine(start, endVertical, Color.blue, 60);
        Debug.DrawLine(start, endHorizontal, Color.blue, 60);
    }

    public static Vector3 Position(RectTransform transform)
    {
        return transform.position;
    }
}