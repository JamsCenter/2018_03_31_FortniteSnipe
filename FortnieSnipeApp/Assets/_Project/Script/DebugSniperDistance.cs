using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/// <summary>
/// Script write by Eloi Stree (DefaultCompany). 
/// Check the following link to acknowledge the license.
/// License: "https://www.patreon.com/jamscenter"
/// Created: 3/31/2018 6:05:02 PM 
/// </summary>
[HelpURL("https://www.patreon.com/jamscenter")]
[ExecuteInEditMode]
public class DebugSniperDistance : MonoBehaviour {

    public Transform _playerPosition;
    
    void Update () {

        DrawDebug();

	}

    private void DrawDebug()
    {
        FortniteDistanceMeasure measure = FortniteDistanceMeasure.InstanceInScene;

        if (measure != null)
            measure = FindObjectOfType<FortniteDistanceMeasure>();

         if (measure != null )
        {
            float unitsByMeter = measure.GetUnityUnitByMeter();
            Vector3 playerPosition = _playerPosition.position;
            DebugDrawCross(playerPosition, unitsByMeter * 300, Color.red);
            DebugDrawCross(playerPosition, unitsByMeter * 200, Color.magenta);
            DebugDrawCross(playerPosition, unitsByMeter * 100, Color.green);
        }
    }

    private void DebugDrawCross(Vector3 playerPosition, float distanceInUnity, Color color)
    {
        Vector3 left, right, up, down;
        left = playerPosition + new Vector3(-distanceInUnity, 0, 0);
        right = playerPosition + new Vector3(distanceInUnity, 0, 0);
        up = playerPosition + new Vector3(0,distanceInUnity,  0);
        down = playerPosition + new Vector3(0,-distanceInUnity, 0);
        Debug.DrawLine(left, right, color, 1f);
        Debug.DrawLine(down, up, color, 1f);
    }

    public void OnValidate()
    {
        DrawDebug();

    }
}
