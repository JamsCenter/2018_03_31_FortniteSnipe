using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/// <summary>
/// NotMyResponsabilityException  is a class send to warn that your are using my script without knowing what you are doing.
/// 
/// Script write by Eloi Stree (DefaultCompany). 
/// Check the following link to acknowledge the license.
/// License: "https://www.patreon.com/jamscenter"
/// Created: 3/31/2018 4:34:10 PM 
/// </summary>
[HelpURL("https://www.patreon.com/jamscenter")]
public class NotMyResponsabilityException : Exception
{
    public NotMyResponsabilityException(string message) : base(message)
    {

    }
}
